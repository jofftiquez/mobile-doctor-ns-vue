import * as firebaseWebApi from 'nativescript-plugin-firebase/app';
import * as ls from 'application-settings';

const init = () => {
  firebaseWebApi.initializeApp({
    persist: true
  });
}

const ref = firebaseWebApi.database().ref('_local_data_');
const IS_LOGGED_IN = 'is-logged-in';
const CURRENT_USER = 'current-user';
const CURRENT_USER_TOKEN = 'current-user-token';
const SELECTED_CLINIC = 'selected-clinic';

export const isLoggedIn = () => {
  return ls.getBoolean(IS_LOGGED_IN);
}

export const setCurrentUser = async (data) => {
  try {
    init();
    const uid = data ? data.uid : ls.getString(CURRENT_USER);
    await ref.child(CURRENT_USER)
      .child(uid)
      .set(data);
    ls.setString(CURRENT_USER, uid);
    ls.setBoolean(IS_LOGGED_IN, data ? true : false);
  } catch (e) {
    console.log('error#firebase - setCurrentUser', e);
    throw e;
  }
}

export const getCurrentUser = async () => {
  try {
    init();
    const uid = ls.getString(CURRENT_USER);
    
    const snap = await ref.child(CURRENT_USER)
      .child(uid)
      .once('value');
    
    return snap.val();
  } catch (e) {
    console.log('error#firebase - getCurrentUser', e);
    throw e;
  }
}

export const setSelectedClinic = async (data) => {
  try {
    init();
    const uid = ls.getString(CURRENT_USER);
    await ref.child(SELECTED_CLINIC)
      .child(uid)
      .set(data)
  } catch (e) {
    console.log('errro#firebase - setCurrentClinic', e);
  }
}

export const getSelectedClinic = async () => {
  try {
    init();
    const uid = ls.getString(CURRENT_USER);
    const snap = await ref.child(SELECTED_CLINIC)
      .child(uid)
      .once('value');
    return snap.val();
  } catch (e) {
    console.log('errro#firebase - getCurrentClinic', e);
  }
}

//////////
export const logout = async () => {
  await setCurrentUser(null);
  await setSelectedClinic(null);
  ls.remove(CURRENT_USER);
  ls.remove(IS_LOGGED_IN);
}

init();















// export const setCurrentUserToken = async (token) => {
//   try {
//     console.log(token)
//     init();
//     const uid = ls.getString(CURRENT_USER);
//     await ref.child(CURRENT_USER_TOKEN)
//       .child(ls.getString(CURRENT_USER))
//       .set(token);
//   } catch (e) {
//     console.log('error#firebase - setCurrentUserToken', e);
//     throw e;
//   }
// }

// export const getCurrentUserToken = async () => {
//   try {
//     init();
//     const uid = ls.getString(CURRENT_USER);
//     const snap = await ref.child(CURRENT_USER_TOKEN)
//       .child(uid)
//       .once('value');
//     console.log(snap.val())
//     return snap.val();
//   } catch (e) {
//     console.log('error#firebase - getCurrentUserToken', e);
//     throw e;
//   }
// }