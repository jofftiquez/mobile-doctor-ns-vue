import Vue from 'nativescript-vue';
import NsVueNami from '../plugins/ns-vue-nami';

import searchSpecialization from '~/components/commons/search-specialization';
import login from '~/components/login';
import forgotPassword from '~/components/forgot-password';
import main from '~/components/main';
import dashboard from '~/components/dashboard';
import patient from '~/components/patient/tab';
import createPatient from '~/components/patient/create';
import createPatientAccount from '~/components/patient/create-account';
import createAppointment from '~/components/appointments/create';
import clinicList from '~/components/clinic/list';
import clinic from '~/components/clinic/create';
import queueList from '~/components/queues/list'
import updateProfile from '~/components/profile/tab';
import chatInbox from '~/components/chat/inbox';
import chatConversation from '~/components/chat/conversation';
import reviewEncounter from '~/components/chat/review-encounter';
import mcDatePicker from '~/components/commons/mc-date-picker';
import mcCountryPicker from '~/components/commons/mc-country-picker';
import mcSearchTags from '~/components/commons/mc-search-tags';
import mcSearchContracts from '~/components/commons/mc-search-contracts';
import mcSearchPatient from '~/components/commons/mc-search-patient';
import searchMedicine from '~/components/commons/mc-search-medicine';
import searchProcedure from '~/components/commons/mc-search-procedure';
import searchLabTest from '~/components/commons/mc-search-lab-test';
import searchImagingTest from '~/components/commons/mc-search-imaging-test';
import searchICD10 from '~/components/commons/mc-search-icd10';
import searchStaff from '~/components/commons/mc-search-staff';

import medRecordsRoutes from './med-records';
import onboardingRoutes from './onboarding';
import signupRoutes from './signup';

import * as localStorage from 'nativescript-localstorage';

Vue.use(NsVueNami, {
  transition: {
    name: 'slide',
    duration: 200
  },
});

const vm = new Vue();

vm.$nami.authGuard(async (next, route) => {
  console.log('#authGuard', localStorage.getItem('IS_LOGGED_IN') === 'YES');
  if(localStorage.getItem('IS_LOGGED_IN') === 'YES') {
    next(route);
  } else {
    next('login');
  }
});

export default vm.$nami.init({
  routes: [
    {
      name: 'login',
      component: login,
      noAuth: true,
      entry: !localStorage.getItem('IS_LOGGED_IN') === 'YES'
    },
    {
      name: 'forgot-password',
      component: forgotPassword,
      noAuth: true
    },
    {
      name: 'main',
      component: main,
      entry: localStorage.getItem('IS_LOGGED_IN') === 'YES'
    },
    {
      name: 'patient',
      component: patient,
    },
    {
      name: 'create-patient',
      component: createPatient,
    },
    {
      name: 'create-patient-account',
      component: createPatientAccount,
    },
    {
      name: 'clinic',
      component: clinic
    },
    {
      name: 'clinic-list',
      component: clinicList,
    },
    {
      name: 'create-appointment',
      component: createAppointment,
    },
    {
      name: 'update-profile',
      component: updateProfile
    },
    {
      name: 'queue-list',
      component: queueList
    },
    {
      name: 'chat-inbox',
      component: chatInbox
    },
    {
      name: 'chat-conversation',
      component: chatConversation
    },
    {
      name: 'review-encounter',
      component: reviewEncounter
    },
    {
      name: 'mc-date-picker',
      component: mcDatePicker
    },
    {
      name: 'mc-country-picker',
      component: mcCountryPicker
    },
    {
      name: 'mc-search-tags',
      component: mcSearchTags
    },
    {
      name: 'mc-search-contracts',
      component: mcSearchContracts
    },
    {
      name: 'mc-search-patient',
      component: mcSearchPatient
    },
    {
      name: 'search-specialization',
      component: searchSpecialization,
      noAuth: true
    },
    {
      name: 'search-medicine',
      component: searchMedicine
    },
    {
      name: 'search-lab-test',
      component: searchLabTest
    },
    {
      name: 'search-imaging-test',
      component: searchImagingTest
    },
    {
      name: 'search-procedure',
      component: searchProcedure
    },
    {
      name: 'search-icd10',
      component: searchICD10
    },
    {
      name: 'search-staff',
      component: searchStaff
    },
    ...medRecordsRoutes,
    ...onboardingRoutes,
    ...signupRoutes
  ]
});
