import step1 from '~/components/signup/step-1.vue';
import step2 from '~/components/signup/step-2.vue';
import step3 from '~/components/signup/step-3.vue';

export default [
  {
    name: 'signup-step-1',
    component: step1,
    noAuth: true
  },
  {
    name: 'signup-step-2',
    component: step2,
    noAuth: true
  },
  {
    name: 'signup-step-3',
    component: step3,
    noAuth: true
  },
];