import docOnboardingStep1 from '~/components/onboarding/step-1';
import docOnboardingStep2 from '~/components/onboarding/step-2';
import docOnboardingStep3 from '~/components/onboarding/step-3';

export default [
  {
    name: 'doc-onboarding-step-1',
    component: docOnboardingStep1,
    noAuth: true
  },
  {
    name: 'doc-onboarding-step-2',
    component: docOnboardingStep2,
    noAuth: true
  },
  {
    name: 'doc-onboarding-step-3',
    component: docOnboardingStep3,
    noAuth: true
  },
];