import createPastMedicalHistory from '~/components/medical-records/medical-history/past-medical-history/create';
import createFamilyHistory from '~/components/medical-records/medical-history/family-history/create';
import createSocialHistory from '~/components/medical-records/medical-history/social-history/create';
import createAllergy from '~/components/medical-records/medical-history/allergy/create';
import createBirthHistory from '~/components/medical-records/medical-history/birth-history/create';
import createGynecologicalHistory from '~/components/medical-records/medical-history/gynecological-history/create';
import createHospitalizationHistory from '~/components/medical-records/medical-history/hospitalization-history/create';
import createMenstrualHistory from '~/components/medical-records/medical-history/menstrual-history/create';
import createObstetricHistory from '~/components/medical-records/medical-history/obstetric-history/create';
import createSurgicalHistory from '~/components/medical-records/medical-history/surgical-history/create';
import createVaccinationHistory from '~/components/medical-records/medical-history/vaccination-history/create';
import createVitals from '~/components/medical-records/vitals/create';
import createROS from '~/components/medical-records/ros/create';
import createCarePlanNotes from '~/components/medical-records/care-plan/create';
import createImpression from '~/components/medical-records/impression/create';
import createDiagnosis from '~/components/medical-records/diagnosis/create';
import createPrescription from '~/components/medical-records/medication-order/create';
import createSpecialtyFeatENT from '~/components/medical-records/specialty-features/ent/create';
import createSpecialtyFeatOB from '~/components/medical-records/specialty-features/ob-gyn/create';
import createPhysicalExam from '~/components/medical-records/physical-exam/create';
import createDiagnosticOrder from '~/components/medical-records/diagnostic-order/create';
import createProcedure from '~/components/medical-records/procedure/create';
import labResults from '~/components/medical-records/diagnostic-results/lab-results';
import imagingResults from '~/components/medical-records/diagnostic-results/imaging-results';


export default [
  {
    name: 'create-past-medical-history',
    component: createPastMedicalHistory
  },
  {
    name: 'create-family-history',
    component: createFamilyHistory
  },
  {
    name: 'create-social-history',
    component: createSocialHistory
  },
  {
    name: 'create-allergy',
    component: createAllergy
  },
  {
    name: 'create-birth-history',
    component: createBirthHistory
  },
  {
    name: 'create-gynecological-history',
    component: createGynecologicalHistory
  },
  {
    name: 'create-hospitalization-history',
    component: createHospitalizationHistory
  },
  {
    name: 'create-menstrual-history',
    component: createMenstrualHistory
  },
  {
    name: 'create-obstetric-history',
    component: createObstetricHistory
  },
  {
    name: 'create-surgical-history',
    component: createSurgicalHistory
  },
  {
    name: 'create-vaccination-history',
    component: createVaccinationHistory
  },
  {
    name: 'create-vitals',
    component: createVitals
  },
  {
    name: 'create-ros',
    component: createROS
  },
  {
    name: 'create-impression',
    component: createImpression
  },
  {
    name: 'create-diagnosis',
    component: createDiagnosis
  },
  {
    name: 'create-care-plan-notes',
    component: createCarePlanNotes
  },
  {
    name: 'create-prescription',
    component: createPrescription
  },
  {
    name: 'create-ent-note',
    component: createSpecialtyFeatENT
  },
  {
    name: 'create-ob-note',
    component: createSpecialtyFeatOB
  },
  {
    name: 'create-pe',
    component: createPhysicalExam
  },
  {
    name: 'create-diagnostic-order',
    component: createDiagnosticOrder
  },
  {
    name: 'create-procedure',
    component: createProcedure
  },
  {
    name: 'lab-results',
    component: labResults
  },
  {
    name: 'imaging-results',
    component: imagingResults
  },
];
