import 'babel-polyfill';
import 'nativescript-localstorage';
import '~/firebase';
import Vue from 'nativescript-vue';
import '~/sdk';
import store from '~/vuex';
import VueDevtools from 'nativescript-vue-devtools';
import entry from '~/router';
import '~/mixins';

if(TNS_ENV !== 'production') {
  Vue.use(VueDevtools, {
    host: '192.168.254.107'
  });
}
// Prints Vue logs when --env.production is *NOT* set while building
// Vue.config.silent = (TNS_ENV === 'production');
Vue.config.silent = true;

Vue.registerElement('RadSideDrawer', () => require('nativescript-ui-sidedrawer').RadSideDrawer);
Vue.registerElement('Fab', () => require('nativescript-floatingactionbutton').Fab);
Vue.registerElement('CardView', () => require('nativescript-cardview').CardView);
Vue.registerElement('CheckBox', () => require('nativescript-checkbox').CheckBox, {
  model: {
    prop: 'checked',
    event: 'checkedChange'
  }
});

new Vue({
  store,
  render: h => h('frame', [h(entry)])
}).$start();
