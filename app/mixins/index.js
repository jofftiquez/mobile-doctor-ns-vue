import Vue from 'nativescript-vue';
import * as Toast from 'nativescript-toast';
import differenceInYears from 'date-fns/difference_in_years';
import differenceInWeeks from 'date-fns/difference_in_weeks';
import differenceInDays from 'date-fns/difference_in_days';
import { addMonths, addDays, format } from 'date-fns';
import { LoadingIndicator } from 'nativescript-loading-indicator';
import { core } from '@mycure/sdk';
import isEmail from 'validator/lib/isEmail';
import { logout } from '~/firebase';
const loader = new LoadingIndicator();

Vue.mixin({
  computed: {
    $medRecordsHelper () {
      return {
        computeEdcByDate (date) {
          if(!date) return '--';
          return format(addDays(addMonths(date, 9), 7), 'MM/DD/YY');
        },
        computeAOGUsingLMP (date) {
          if(!date) return '--';

          let weeks = differenceInWeeks(new Date(), date);
          
          let days = differenceInDays(new Date(), date);

          if (!weeks && !days) return '--';

          if (!weeks) {
            let desc = days == 1 ? 'day' : 'days';
            return `${ days } ${ desc }`;
          }

          if (weeks && !days) {
            let desc = weeks == 1 ? 'week' : 'weeks';
            return `${ weeks } ${ desc }`;
          }

          return `${ weeks } w / ${ days } d`;
        },
        computeEDCUsingUS (date, aog) {
          if(!date || !aog) return '--';
          let days = 280 - aog;
          return format(addDays(date, days), 'MM/DD/YY');
        }
      }
    }
  },
  methods: {
    $twoDigits (num) {
      if(num.toString().length === 1) 
        return `0${num}`;
      return num;
    },
    $error () {

    },
    $showAlert (title, message, okButtonText = 'OK') {
      alert({
        title,
        message,
        okButtonText
      });
    },
    async $showConfirm(message = '', title = 'Confirm', okButtonText = 'Yes', cancelButtonText = 'No') {
      return confirm({ title, message, okButtonText, cancelButtonText });
    },
    $sessionExpired () {
      return alert({
        cancelable: false,
        title: 'Session Expired',
        message: 'Your session has expired. Log out and re-log in to continue using MYCURE.',
        okButtonText: 'Log out'
      });
    },
    $parseName (name, placeholder = 'Name Here') {
      const { firstName, middleName, lastName } = name;
      let comma = '';
      let formatted = '';
      if(firstName && lastName) {
        comma = ',';
        formatted = `${lastName}${comma} ${firstName} ${middleName ? middleName.substr(0,1).toUpperCase() : ''}`;
      } else {
        formatted = placeholder;
      }
      return formatted;
    },
    $parseAge (date) {
      if(!date) return 0;
      return differenceInYears(new Date(), date);
    },
    $formatDate (date, frmt = 'MM-DD-YYYY hh:mm A') {
      if(!date) return 'No date';
      return format(date, frmt);
    },
    $showToast (message = 'Hello World!') {
      console.log('toast', message);
      Toast.makeText(message, 'long').show();
    },
    $isObjEmpty (obj) {
      for(var key in obj) {
        if(obj.hasOwnProperty(key))
          return true;
      }
      return false;
    },
    $dialogLoader () {
      return {
        show() {
          console.log('$dialogLoader()', 'SHOW');
          loader.show();
        },
        hide() {
          console.log('$dialogLoader()', 'HIDE');
          loader.hide();
        }
      }
    },
    $loadingIndicatorOptions () {
      return {
        message: 'Loading...',
        color: '#0099cc',
        android: {
          indeterminate: true,
          cancelable: true
        }
      }
    }
  }
})