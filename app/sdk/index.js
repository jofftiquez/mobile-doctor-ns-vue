import { core } from '@mycure/sdk';

const init = () => {
  core.initialize(process.env.BASE_URL);
}

init();