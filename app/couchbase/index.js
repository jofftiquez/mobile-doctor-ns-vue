const couchbase = require('nativescript-couchbase');
const localStorage = require('application-settings');

const db = new couchbase.Couchbase('mycure-db');
const CURR_USER = 'curr-user';
const CURR_ACTIVE_ORG = 'curr-active-org';
const TOKEN = 'access-token';

function _createAccessToken(token) { 
  localStorage.setString(TOKEN, token);
}

function _deleteToken() {
  localStorage.remove(TOKEN);
}

// user
function _setCurrentUserId(id) {
  localStorage.setString(CURR_USER, id);
}

function _getCurrentUserId() {
  return localStorage.getString(CURR_USER) || 'none';
}

function _removeCurrentUserId() {
  localStorage.remove(CURR_USER);
}

// clinic

function _setCurrentClinicId(id) {
  localStorage.setString(CURR_ACTIVE_ORG, id);
}

function _getCurrentClinicId() {
  return localStorage.getString(CURR_ACTIVE_ORG) || 'none';
}

function _removeCurrentClinicId() {
  localStorage.remove(CURR_ACTIVE_ORG);
}

export const getToken = () => {
  const token = localStorage.getString(TOKEN);
  return token;
}

export const createToken = (token) => {
  _createAccessToken(token);
}

// user

export const createCurrentUserData = (data) => {
  const id = db.createDocument(data);
  _setCurrentUserId(id);
  
}

export const getCurrentUserData = () => {
  const data = db.getDocument(_getCurrentUserId());
  return data;
}

export const deleteCurrentUserData = () => {
  const isDeleted =  db.deleteDocument(_getCurrentUserId());
  if(isDeleted) {
    _removeCurrentUserId();
    return true;
  }
  return false;
}


// clinic

export const createCurrentClinicData = (data) => {
  const id = db.createDocument(data);
  _setCurrentClinicId(id);
}

export const getCurrentClinicData = () => {
  const data = db.getDocument(_getCurrentClinicId());
  return data;
}

export const deleteCurrentClinicData = () => {
  const isDeleted =  db.deleteDocument(_getCurrentClinicId());
  if(isDeleted) {
    _removeCurrentClinicId();
    return true;
  }
  return false;
}


// destroy

export const destroy = () => {
  deleteCurrentUserData();
  deleteCurrentClinicData();
  _deleteToken();
  localStorage.clear();
  // db.destroyDatabase();
}