import { getRoute } from '../utils';
let routes = [];
let guard = null;
let vm = null;
let currentRoute = '';
let globalTransition = null;

export const init = (options) => {
  routes = options.routes || [];
  let entry = routes.find(route => route.entry);
  if(!entry) {
    console.warn('No entry component sepecified. Deafaulting to first component in routes array.');
    entry = routes[0];
  }
  navigate(entry.name);
  return entry.component;
}

export const register = (route) => {
  routes.push(route);
}

export const current = () => {
  return getRoute(currentRoute, routes);
}

export const navigate = async (routeName, options) => {
  try {
    let { name, component, meta, noAuth } = getRoute(routeName, routes);
    let route = null;

    if(!noAuth) {
      route = await new Promise((resolve) => guard(resolve));
    }

    if(route) {
      currentRoute = route;
      vm.$nami.navigate(route);
    } else {
      currentRoute = name;
      
      let props = options ? options.props : null;

      // Default global transition
      let transition = globalTransition ? globalTransition.transition : null;
      let transitionAndroid = globalTransition ? globalTransition.transitionAndroid : null;
      let transitionIOS = globalTransition ? globalTransition.transitionIOS : null;

      // override if transitions in options 
      // is present
      if(options && options.transition) {
        transition = options.transition;
      }
      
      if(options && options.transitionAndroid) {
        transitionAndroid = options.transitionAndroid;
      }
      
      if(options && options.transitionIOS) {
        transitionIOS = options.transitionIOS;
      }

      console.log(transition);
      
      vm.$navigateTo(component, {props});
    }
  } catch (error) {
    console.log('navigate error', error);
  }
}

export const modal = (name, props) => {
  const { component } = getRoute(name);
  vm.$showModal(component, {props});
}

export const back = () => {
  vm.$navigateBack();
}

// Hooks
export const authGuard = (fn) => {
  guard = fn;
}

export const nami = (vi, options) => {
  vm = vi;
  let { transition, transitionAndroid, transitionIOS } = options;
  globalTransition = {
    transition, transitionAndroid, transitionIOS
  }
  return {
    init,
    register,
    current,
    navigate,
    modal,
    back,
    authGuard
  }
}