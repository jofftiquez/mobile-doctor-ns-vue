import { core } from '@mycure/sdk';
import _ from 'lodash';

const state = {
  appointments: [],
  newAppointmentStartDate: null,
  newAppointmentEndDate: null,
  pendingAppointments: [],
  confirmedAppointments: [],
  cancelledAppointments: []
};

const getters = {
  newAppointmentStartDate: (s) => s.newAppointmentStartDate,
  newAppointmentEndDate: (s) => s.newAppointmentEndDate,
  appointments: (s) => s.appointments,
  pendingAppointments: (s) => s.pendingAppointments,
  confirmedAppointments: (s) => s.confirmedAppointments,
  cancelledAppointments: (s) => s.cancelledAppointments
};

const actions = {
  createAppointment: async ({commit}, payload) => {
    await core.appointments().create(payload);
  },
  getAppointments: async ({commit}, {uid}) => {
    const query = {
      $populate: {
        patient: {
          service: 'personal-details', key: 'meta.patient'
        },
        queueItem: {
          service: 'queue-items', key: 'id', 
          method: 'findOne', idField: 'meta.appointment'
        }
      },
      $sort: {
        startAt: -1,
      },
      target: {
        $in: [uid]
      }
    }

    commit('setAppointments', []);
    commit('setPendingAppointments', []);
    commit('setConfirmedAppointments', []);
    commit('setCancelledAppointments', []);

    const { items } = await core.appointments().find(query);

    items.forEach(item => {
      const patient = item.$populated.patient;
      const queueItem = item.$populated.queueItem;

      commit('pushAppointment', {
        ..._.omit(item, ['$populated']),
        patient,
        queueItem
      });

      if(!item.cancelledAt && !item.arrivedAt) {
        commit('pushPendingAppointment', {
          ..._.omit(item, ['$populated']),
          patient,
          queueItem
        });
      }
      
      if(item.arrivedAt && !item.cancelledAt) {
        commit('pushConfirmedAppointment', {
          ..._.omit(item, ['$populated']),
          patient,
          queueItem
        });
      }

      if(item.cancelledAt) {
        commit('pushCancelledAppointment', {
          ..._.omit(item, ['$populated']),
          patient,
          queueItem
        });
      }
    });
  },
  updateAppointment: async ({commit}, {id, payload}) => {
    await core.appointments().update(id, _.pick(payload, ['target', 'startAt', 'endAt', 'reason', 'meta']));
  },
  confirmAppointment: async ({commit}, {id}) => {
    await core.appointments().update(id, {arrive: true});
  },
  cancelAppointment: async ({commit}, {id}) => {
    await core.appointments().update(id, {cancel: true});
  },
};

const mutations = {
  setAppointments: (s, val) => {
    s.appointments = val;
  },
  pushAppointment: (s, val) => {
    s.appointments.push(val);
  },
  pushPendingAppointment: (s, val) => {
    s.pendingAppointments.push(val);
  },
  setPendingAppointments: (s, val) => {
    s.pendingAppointments = val;
  },
  pushConfirmedAppointment: (s, val) => {
    s.confirmedAppointments.push(val);
  },
  setConfirmedAppointments: (s, val) => {
    s.confirmedAppointments = val;
  },
  pushCancelledAppointment: (s, val) => {
    s.cancelledAppointments.push(val);
  },
  setCancelledAppointments: (s, val) => {
    s.cancelledAppointments = val;
  },
  setNewAppointmentStartDate: (s, val) => {
    s.newAppointmentStartDate = val;
  },
  setNewAppointmentEndDate: (s, val) => {
    s.newAppointmentEndDate = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
