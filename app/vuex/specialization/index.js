
const state = {
  selectedFromSearch: []
};

const getters = {
  selectedFromSearch: (s) => s.selectedFromSearch
};

const actions = {};

const mutations = {
  setSelectedFromSearch: (s, val) => {
    s.selectedFromSearch = val;
  },
  removeItemFromSelectedFromSearch: (s, index) => {
    s.selectedFromSearch.splice(index, 1);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
