import Vue from 'nativescript-vue';
import Vuex from 'vuex';
import analytics from './analytics';
import appointment from './appointment';
import auth from './auth';
import clinic from './clinic';
import clock from './clock';
import dash from './dashboard';
import encounter from './encounter';
import fixture from './fixture';
import diagnostic from './diagnostic';
import medicines from './medicines';
import medRecord from './medical-record';
import user from './user';
import patient from './patient';
import procedure from './procedure';
import queue from './queue';
import spec from './specialization';
import searchMed from './search-med';
import ui from './ui';
import '~/couchbase';

Vue.use(Vuex);
Vue.config.devtools = true;

const debug = process.env.NODE_ENV !== 'production';

const store = new Vuex.Store({
  modules: {
    analytics,
    appointment,
    auth,
    clinic,
    clock,
    dash,
    encounter,
    fixture,
    diagnostic,
    medicines,
    medRecord,
    user,
    patient,
    procedure,
    queue,
    spec,
    searchMed,
    ui,
  },
  strict: debug
});

Vue.prototype.$store = store;

module.export = store;
