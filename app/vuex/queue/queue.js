import { core } from '@mycure/sdk';

const state = {
  queues: [],
  selectedQueue: {
    queue: null,
    queueChanged: false
  },
  queueItems: [],
  postponedQueueItems: [],
  nowServing: null
};

const getters = {
  queues: (s) => s.queues,
  queueItems: (s) => s.queueItems,
  postponedQueueItems: (s) => s.postponedQueueItems,
  nowServing: (s) => s.nowServing,
  selectedQueue: (s) => s.selectedQueue
};

const actions = {
  getQueues: async ({commit, state}, {organization}) => {
    const query = {
      organization,
      $and: [
        { 
          type: { 
            $nin: ['cashier-pool', 'end-of-encounter']
          }
        },
        { type: { 
            $in: ['doctor'] 
          }
        }
      ]
    };
    let { items } = await core.queue.queues().find(query);
    commit('setQueues', items);
    if(!state.selectedQueue.queue) {
      commit('setSelectedQueue', items[0]);
    }
  },
  getQueueItems: async ({commit, dispatch}, {organization, queue}) => {
    const query = {
      organization, 
      queue,
      $active: true
    }
    let { items } = await core.queue.items().find(query);
    console.log('queue items', items);
    items = items.sort((a, b) => a.order - b.order);
    // first element of array is the nowServing
    commit('setNowServing', items[0]);
    // remove the first element
    items.shift();
    commit('setQueueItems', items);
    await dispatch('getPostponed', {organization, queue});
    commit('setSelectedQueueChanged', false);
  },
  getPostponed: async ({commit}, {organization, queue}) => {
    const query = {
      organization, 
      queue,
      $deferred: true
    }
    const { items } = await core.queue.items().find(query);
    commit('setPostponed', items);
  },
  updateStatus: async ({commit}, {updateType, item}) => {
    // defer
    if(updateType === 'postpone')
      await core.queue.items().update(item.id, {defer: true});
    // dequeue
    if(updateType === 'skip')
      await core.queue.items().update(item.id, {finish: true});
    if(updateType === 'requeue')
      await core.queue.items().update(item.id, {requeue: true});
    // delete
    if(updateType === 'delete')
      await core.queue.items().remove(item.id);
  },
  accommodateQueueItem: async ({commit}, id) => {
    await core.queue.items().update(id, {start: true});
  }
};

const mutations = {
  setQueues: (s, val) => {
    s.queues = val;
  },
  setSelectedQueue: (s, val) => {
    s.selectedQueue.queue = val;
  },
  setSelectedQueueChanged: (s, val) => {
    s.selectedQueue.queueChanged = val;
  },
  setQueueItems: (s, val) => {
    s.queueItems = val;
  },
  setNowServing: (s, val) => {
    s.nowServing = val;
  },
  setPostponed: (s, val) => {
    s.postponedQueueItems = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
