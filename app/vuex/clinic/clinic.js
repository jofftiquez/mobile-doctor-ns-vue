import { core } from '@mycure/sdk';
import {
  setSelectedClinic,
  getSelectedClinic
} from '~/firebase';
import _ from 'lodash';

const state = {
  selectedClinic: {
    name: ''
  },
  clinicPxCount: 0,
  clinicQueueCount: 0,
  clinicAppointmentsCount: 0,
  clinics: [],
  insuranceContracts: [],
  searchClinicResults: [],
  refreshClinicList: false,
  clinicMembers: []
};

const getters = {
  selectedClinic: (s) => s.selectedClinic,
  clinics: (s) => s.clinics,
  searchClinicResults: (s) => s.searchClinicResults,
  clinicPxCount: (s) => s.clinicPxCount,
  clinicQueueCount: (s) => s.clinicQueueCount,
  clinicAppointmentsCount: (s) => s.clinicAppointmentsCount,
  refreshClinicList: (s) => s.refreshClinicList,
  clinicMembers: (s) => s.clinicMembers
};

const actions = {
  getClinicMembers: async ({commit}, {types, facility:organization}) => {
    const query = {
      organization
    }

    if(types && types.length) {
      query.roles = {
        $in: types
      }
    }

    const { items } = await core.organization.members().find(query);
    commit('setClinicMembers', items);
  },
  getClinics: async ({commit, dispatch}) => {
    const clinics = [];

    const { uid } = await core.auth().currentUser();

    const query = {
      uid,
      $populate: {
        org: {
          service: 'organizations',
          key: 'organization'
        }
      }
    };

    const { items } = await core.organization
      .members()
      .find(query);
    
    let selectedClinic = undefined;

    items.forEach(item => {
      if(item.$populated.org) {
        clinics.push(item.$populated.org);
      }
    });

    commit('setClinics', clinics);

    selectedClinic = await getSelectedClinic();

    if(clinics.length >= 1 && !selectedClinic) {
      console.log('No selected clinic yet.');
      selectedClinic = clinics[0];
      await setSelectedClinic(selectedClinic);
      await dispatch('getSelectedClinicData', selectedClinic.id);
    }
    commit('setSelectedClinic', selectedClinic);
    return selectedClinic;
  },
  getInsuranceContracts: async ({state, commit}, searchText) => {
    const { selectedClinic } = state;
    if (!selectedClinic) throw Error('No selected clinic.');

    const searchQuery = searchText
      ? {insurerName: {$regex: searchText, $options: 'i'}}
      : {};
    const query = {
      insured: selectedClinic.id,
      ...searchQuery,
      $populate: {
        hmo: {
          service: 'organizations',
          method: 'get',
          localKey: 'insurer'
        }
      }
    };

    const { items } = await core.insurance.contracts().find(query);
    const insuranceContracts = items.map(contract => ({
      ..._.omit(contract, '$populated'),
      ...contract.$populated
    }));

    commit('setInsuranceContracts', insuranceContracts);
  },
  searchClinic: async ({commit}, payload) => {
    const data = [];
    commit('setSearchClinicResults', data);
  },
  setSelectedClinic: async ({commit}, data) => {
    await setSelectedClinic(data);
    commit('setSelectedClinic', data);
  },
  getSelectedClinic: async ({commit}) => {
    const data = await getSelectedClinic();
    commit('setSelectedClinic', data);
    return data;
  },
  getSelectedClinicData: async ({commit, dispatch}, {id, uid}) => {
    dispatch('getClinicPxCount', id);
    dispatch('getClinicInQueueCount', id);
    dispatch('getClinicAppointmentsCount', {id, uid});
    dispatch('analytics/getPatientVisit', {facility: id}, {root: true});
    dispatch('analytics/getSexSummary', {facility: id}, {root: true});
    dispatch('analytics/getSexTransactionSummary', {facility: id}, {root: true});
  },
  getClinicPxCount: async ({commit}, id) => {
    const query = {
      facility: id
    };
    const total = await core.medical.patients().count(query);
    commit('setClinicPxCount', total);
  },
  getClinicInQueueCount: async ({commit}, id) => {
    const query = {
      organization: id,
      $active: true
    };
    const total = await core.queue.items().count(query);
    console.log(total);
    commit('setClinicQueueCount', total);
  },
  getClinicAppointmentsCount: async ({commit}, {id, uid}) => {
    // get pending appointments
    const query = {
      organization: id,
      target: uid,
      cancelledAt: {
        $exists: false
      },
      arrivedAt: {
        $exists: false
      }
    };
    const total = await core.appointments().count(query);
    commit('setClinicAppointmentsCount', total);
  },
  updateClinic: async ({commit}, payload) => {
    const id = payload.id;
    let data = _.pickBy(_.omit(payload, ['id']));
    const updatedClinic = await core.organizations().update(id, data);
    return updatedClinic;
  },
  createClinic: async ({commit}, payload) => {
    const data = {
      type: 'personal-clinic',
      ...payload
    }
    const clinic = await core.organizations().create(data);
    commit('unshiftClinic', clinic);
  },
  switchClinic: async ({commit}, payload) => {
    commit('setSelectedClinic', payload);
  }
};

const mutations = {
  setClinics: (s, val) => {
    s.clinics = val;
  },
  setInsuranceContracts: (s, val) => {
    s.insuranceContracts = val;
  },
  unshiftClinic: (s, val) => {
    s.clinics.unshift(val);
  },
  setRefreshClinicList: (s, val) => {
    s.refreshClinicList = val;
  },
  setSearchClinicResults: (s, val) => {
    s.searchClinicResults = val;
  },
  setClinicPxCount: (s, val) => {
    s.clinicPxCount = val;
  },
  setClinicQueueCount: (s, val) => {
    s.clinicQueueCount = val;
  },
  setClinicAppointmentsCount: (s, val) => {
    s.clinicAppointmentsCount = val;
  },
  setSelectedClinic: (s, val) => {
    s.selectedClinic = val;
  },
  setClinicMembers: (s, val) => {
    s.clinicMembers = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
