const state = {
  onboardingProgress: 1,
  onboardingTotalSteps: 3,
};

const getters = {
  onboardingProgress: (s) => s.onboardingProgress,
  onboardingTotalSteps: (s) => s.onboardingTotalSteps,
};

const actions = {};

const mutations = {
  setOnboardingProgress: (s, val) => {
    s.onboardingProgress = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
