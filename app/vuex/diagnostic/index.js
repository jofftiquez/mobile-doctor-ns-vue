import { core } from '@mycure/sdk';

const state = {
  labTests: [],
  imagingTests: [],
};

const getters = {
  labTests: (s) => s.labTests,
  imagingTests: (s) => s.imagingTests
};

const actions = {
  getDiagnosticTests: async ({commit}, {type, facility}) => {
    const query = {
      type,
      $or: [
        { facility }
      ]
    };

    const { items } = await core.diagnostic.tests().find(query);

    if(type === 'laboratory')
      commit('setLabTests', items);

    if(type === 'radiology')
      commit('setImagingTests', items);
  }
};

const mutations = {
  setLabTests: (s, val) => {
    s.labTests = val;
  },
  setImagingTests: (s, val) => {
    s.imagingTests = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
