import { core } from '@mycure/sdk';
import isEmail from 'validator/lib/isEmail';
import { 
  setCurrentUser, 
  getCurrentUser,
  logout
} from '~/firebase';

const state = {
  currentUser: null,
  userMemberships: null
};

const getters = {
  currentUser: async (s) => s.currentUser,
  userMemberships: (s) => s.userMemberships
};

const actions = {
  login: async ({commit}, {email, password}) => {
    console.log(email, password);
    const user = await core.auth().signin('local', {email, password});
    if (!user.isDoctor) { throw new Error('not-doctor'); }
    localStorage.setItem('IS_LOGGED_IN', 'YES');
    await setCurrentUser(user);
  },
  getCurrentUser: async ({commit}) => {
    return core.auth().currentUser();
  },
  logout: async ({commit}) => {
    await core.auth().signout();
    await logout();
    localStorage.clear();
  },
  forgotPassword: async ({commit}, email) => {
    if (!isEmail(email)) { throw new Error('invalid-email'); }
    await core.auth().sendPasswordResetEmail(email);
  },
  changePassword: async ({commit}) => {
    // TODO: change password
  },
  changeEmail: async ({commit}) => {
    // TODO: change email
  },
  signUpMobile: async ({commit}, mobile) => {
    console.log(mobile)
  },
  wipe: ({commit}) => {
    console.log('Wiping auth store...');
    commit('setCurrentUser', null);
    commit('setUserMembership', null);
  }
};

const mutations = {
  setCurrentUser: (s, val) => {
    s.currentUser = val;
  },
  setUserMembership: (s, val) => {
    s.userMemberships = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
