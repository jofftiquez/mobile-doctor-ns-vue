import { core } from '@mycure/sdk';

const state = {
  icd10Defaults: [],
};

const getters = {
  icd10Defaults: (s) => s.icd10Defaults,
};

const actions = {
  getICD10Defaults: async ({commit}, name = '') => {
    const query = {
      type: 'icd10',
      $or: [
        { id: { $regex: `${name}`, $options: `i` } },
        { text: { $regex: `${name}`, $options: `i` } }
      ]
    };

    commit('setICD10Defaults', []);
    const { items } = await core.fixtures().find(query);
    commit('setICD10Defaults', items);
  }
};

const mutations = {
  setICD10Defaults: (s, val) => {
    s.icd10Defaults = val;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
