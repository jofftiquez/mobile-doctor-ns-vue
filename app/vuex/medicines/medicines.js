import { core } from '@mycure/sdk';

const state = {
  medicines: [],
  medicinesDefaults: [],
  medicinesDefaultsList: [],
  selectedMedicine: null
};

const getters = {
  medicines: (s) => s.medicines,
  medicinesDefaults: (s) => s.medicinesDefaults,
  medicinesDefaultsList: (s) => s.medicinesDefaultsList,
  selectedMedicine: (s) => s.selectedMedicine
};

const actions = {
  getMedicines: async ({commit}, {organization, uid}) => {
    const query = {
      organization,
      createdBy: uid,
      // $or: [
      //   { genericName: { $regex: `${name}`, $options: `i` }, organization },
      //   { genericName: { $regex: `${name}`, $options: `i` }, createdBy: uid },
      //   { genericName: { $regex: `${name}`, $options: `i` }, createdBy: { $exists: false } },
      //   { brandName: { $regex: `${name}`, $options: `i` }, organization  },
      //   { brandName: { $regex: `${name}`, $options: `i` }, createdBy: uid },
      //   { brandName: { $regex: `${name}`, $options: `i` }, createdBy: { $exists: false } },
      // ],
      $populate: {
        formulations: {
          service: 'medicine-formulations',
          method: 'find',
          foreignKey: 'medicine',
          localKey: 'id'
        }
      }
    };

    commit('setMedicinesDefaults', []);
    const { items } = await core.medicine.medicines().find(query);
    commit('setMedicinesDefaults', items);
  },
  searchMedicines: async ({commit}, {organization, uid, searchText}) => {
    const query = {
      $or: [
        { genericName: { $regex: `${searchText}`, $options: `i` } },
        { brandName: { $regex: `${searchText}`, $options: `i` } },
      ],
      $populate: {
        formulations: {
          service: 'medicine-formulations',
          method: 'find',
          foreignKey: 'medicine',
          localKey: 'id'
        }
      }
    };

    commit('setMedicinesDefaults', []);
    const { items } = await core.medicine.medicines().find(query);
    commit('setMedicinesDefaults', items);
  }
};

const mutations = {
  setMedicines: (s, val) => {
    s.medicines = val;
  },
  setMedicinesDefaults: (s, val) => {
    s.medicinesDefaults = val;
  },
  setMedicinesDefaultsList: (s, val) => {
    s.medicinesDefaultsList = val;
  },
  setSelectedMedicine: (s, val) => {
    s.selectedMedicine = val;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
