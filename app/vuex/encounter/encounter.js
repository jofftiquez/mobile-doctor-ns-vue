import { core } from '@mycure/sdk';
import _ from 'lodash';

const state = {
  currentEncounter: null,
  pastEncounters: []
};

const getters = {
  currentEncounter: (s) => s.currentEncounter,
  pastEncounters: (s) => s.pastEncounters
};

const actions = {
  createEncounter: async ({commit}, {patient, facility}) => {
    const data = {
      type: 'outpatient',
      patient,
      facility
    }

    const encounter = await core.medical.encounters().create(data);
    commit('setCurrentEncounter', encounter);
  },
  getCurrentEncounter: async ({commit}, patient) => {
    const query  = {
      patient,
      finishedAt: {
        $exists: false
      }
    }
    commit('setCurrentEncounter', null);
    const data = await core.medical.encounters().findOne(query);
    commit('setCurrentEncounter', data);
  },
  getPastEncounters: async ({commit}, patient) => {
    const query  = {
      patient,
      $sort: { 'createdAt': -1 },
      finishedAt: {
        $exists: true
      },
      $populate: {
        medicalRecords: {
          service: 'medical-records',
          idField: 'encounter',
          method: 'find',
          key: 'id',
          $populate: {
            createdByDetails: {
              service: 'personal-details',
              idField: 'id',
              method: 'findOne',
              key: 'createdBy',
            },
            createdByMembership: {
              service: 'organization-members',
              idField: 'uid',
              method: 'findOne',
              key: 'createdBy',
            },
            permissions: {
              service: 'permissions',
              method: 'find', 
              localKey: 'id',
              foreignKey: 'subject'
            },
            medicalProcedureProviders: {
              service: 'personal-details',
              method: 'find',
              localKey: 'providers',
              foreignKey: 'id'
            },
            ref: {
              service: 'services',
              method: 'get',
              localKey: 'ref'
            },
            results: {
              service: 'medical-records',
              method: 'find',
              localKey: 'id',
              foreignKey: 'order',
              type: {
                $in: ['lab-test-result', 'imaging-test-result']
              },
              $populate: {
                tests: {
                  service: 'diagnostic-tests',
                  method: 'find',
                  localKey: 'results',
                  extractKey: 'test',
                  foreignKey: 'id'
                },
                measures: {
                  service: 'diagnostic-measures',
                  method: 'find',
                  localKey: 'results',
                  extractKey: 'measure',
                  foreignKey: 'id'
                }
              }
            }
          }
        }
      }
    }

    commit('setPastEncounters', []);

    const { items } = await core.medical.encounters().find(query);

    let encounters = [];

    items.forEach(item => {
      let encounter = item;
      encounter.medicalRecords = _.groupBy(item.$populated.medicalRecords, 'type');
      encounters.push(_.omit(encounter, ['$populated']));
    });
    
    commit('setPastEncounters', items);
  },
  finishEncounter: async ({ dispatch, commit }, encounter) => {
    await core.medical.encounters().update(encounter.id, { finish: true });
    commit('setCurrentEncounter', null);
    await dispatch('getPastEncounters', encounter.patient);
  }
};

const mutations = {
  setCurrentEncounter: (s, val) => {
    s.currentEncounter = val;
  },
  setPastEncounters: (s, val) => {
    s.pastEncounters = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
