const state = {
  refreshDashboard: false
};

const getters = {
  refreshDashboard: (s) => s.refreshDashboard
};

const actions = {};

const mutations = {
  setRefreshDashboard: (s, val) => {
    s.refreshDashboard = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
