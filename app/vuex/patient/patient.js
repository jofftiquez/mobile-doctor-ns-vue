import { core } from '@mycure/sdk';

const state = {
  patients: [],
  selectedPatient: null,
  searchPatientResults: [],
  searchSelectedPatient: {}
};

const getters = {
  patients: (s) => s.patients,
  selectedPatient: (s) => s.selectedPatient,
  searchPatientResults: (s) => s.searchPatientResults,
  searchSelectedPatient: (s) => s.searchSelectedPatient
};

const actions = {
  createPatient: async ({dispatch, commit, getters}, {facility, personalDetails}) => {
    const payload = {
      facility,
      personalDetails
    };

    const data = await core.medical.patients().create(payload);
    await dispatch('getPatients', facility);
    return data;
  },
  updatePatient: async ({commit}, patient) => {
    const id = patient.id;
    const data = await core.personalDetails().update(id, _.omit(patient, ['id']));
    return data;
  },
  archivePatient: async ({commit}, patient) => {
    await core.medical.patients().update(patient, { archive: true });
  },
  getPatients: async ({commit}, facility) => {
    // try {
      let query = {
        facility, 
        archivedAt: { $exists: false },
        $sort: {
          createdAt: -1
        },
        $limit: 20,
        $populate: {
          personalDetails: {
            service: 'personal-details',
            method: 'findOne',
            key: 'id'
          },
          medicalNote: {
            service: 'medical-records',
            method: 'findOne',
            localKey: 'id',
            foreignKey: 'patient',
            facility
          },
          insuranceCards: {
            
          }
        }
      };
  
      const { items } = await core.medical.patients().find(query);
      const patients = [];

      items.forEach(item => {
        let patient = {
          ...item,
          ...item.$populated
        }
        patients.push(_.omit(patient, ['$populated']));
      });

      commit('setPatients', patients);
  },
  getPatient: async ({commit}, id) => {
    const patient = await core.personalDetails().findOne(id);
    // commit('setSelectedPatient', patient);
  },
  searchPatient: async ({commit}, {facilityIds, queryString}) => {
    let query = {
      facility: { $in: facilityIds },
      archivedAt: { $exists: false },
      removedAt: { $exists: false },
      $or: [
        { 'name.firstName': { $regex: `${queryString}`, $options: `i` } },
        { 'name.lastName': { $regex: `${queryString}`, $options: `i` } },
      ],
      $populate: {
        personalDetails: {
          service: 'personal-details',
          method: 'findOne',
          key: 'id'
        }
      }
    };

    const res = await core.personalDetails().find(query);
    commit('setPatients', res.items);
    commit('setSearchPatientResults', res.items);
    return res.items;
  },
  createAccount: async ({commit}, {id, account, personalDetails}) => {
    const existingPatient = await core.medical
      .patients()
      .get(id);

    if(existingPatient.account) {
      throw new Error('This patient is already connected to a MYCURE account.');
    }

    const {
      email,
      password
    } = account;

    const accountAvailable = await core.auth()
      .checkUniqueIdentity('email', email);

    if(!accountAvailable) {
      throw new Error(`An account with email ${email} already exists. Please use another email address.`);
    }

    const newAccount = await core.accounts()
      .create({ 
        email, 
        password, 
        personalDetails 
      });
    
    await core.medical
      .patients()
      .update(id, { 
        account: newAccount.uid 
      });
  }
};

const mutations = {
  setPatients: (s, val) => {
    s.patients = val;
  },
  setSelectedPatient: (s, val) => {
    s.selectedPatient = val;
  },
  setSearchPatientResults: (s, val) => {
    s.searchPatientResults = val;
  },
  appendPxPatient: (s, val) => {
    s.patients.push(val);
  },
  setSearchSelectedPatient: (s, val) => {
    s.searchSelectedPatient = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
