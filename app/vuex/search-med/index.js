const state = {
  medSearchResults: []
};

const getters = {
  medSearchResults: (s) => s.medSearchResults,
};

const actions = {
  searchMed: async ({commit}) => {
    
  }
};

const mutations = {
  setMedSearchResults: (s, val) => {
    s.medSearchResults = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
