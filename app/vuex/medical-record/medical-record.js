import { core } from '@mycure/sdk';
import _ from 'lodash';

const state = {
  medicalHistories: [],
  'chief-complaint': {text: ''},
  hpi: {text: ''},
  ros: [],
  vitals: [],
  specialtyFeatures: [],
  impressions: [],
  diagnosis: [],
  procedures: [],
  physicalExams: [],
  carePlans: [],
  medicationOrders: [],
  labOrders: [],
  imagingOrders: [],
  selectedMedicationOrderMedicines: [],
  medicalHistoryTypes: [
    'Past Medical History',
    'Family History',
    'Social History',
    'Allergies', 
    'Birth History',
    'Gynecologic History',
    'Hospitalization History',
    'Immunizations/Vaccinations',
    'Menstrual History',
    'Obstetric History',
    'Surgical History'
  ],
};

const getters = {
  chiefComplaint: (s) => s['chief-complaint'],
  hpi: (s) => s.hpi,
  medicalHistories: (s) => s.medicalHistories,
  medicalHistoryTypes: (s) => s.medicalHistoryTypes,
  vitals: (s) => s.vitals,
  specialtyFeatures: (s) => s.specialtyFeatures,
  ros: (s) => s.ros,
  impressions: (s) => s.impressions,
  diagnosis: (s) => s.diagnosis,
  procedures: (s) => s.procedures,
  physicalExams: (s) => s.physicalExams,
  carePlans: (s) => s.carePlans,
  medicationOrders: (s) => s.medicationOrders,
  diagnosticOrders: s => _.chain([])
    .concat(s.labOrders, s.imagingOrders)
    .sortBy('createdAt')
    .value(),
  selectedMedicationOrderMedicines: (s) => s.selectedMedicationOrderMedicines
};

const actions = {
  getCurrentEncounterHPIAndChiefComplaint: async ({commit}, {encounter}) => {
    const query = {
      encounter,
      $or: [
        {type: 'chief-complaint'},
        {type: 'hpi'}
      ]
    }
    const { items } = await core.medical.records().find(query);
    items.forEach(record => {
      commit('setMedicalRecord', record);
    });
  },
  getCurrentEncounterMedicalHistories: async ({commit, state}, {encounter}) => {
    const query = {
      encounter,
      $or: [
        {type: 'medical-history'},
        {type: 'family-history'},
        {type: 'social-history'},
        {type: 'allergy'},
        {type: 'birth-history'},
        {type: 'gynecological-history'},
        {type: 'hospitalization-history'},
        {type: 'menstrual-history'},
        {type: 'obstetric-history'},
        {type: 'surgical-history'},
        {type: 'vaccination'}
      ]
    }

    const { items } = await core.medical.records().find(query);
    commit('setMedicalHistories', items);
  },
  getCurrentEncounterSpecialtyFeatures: async ({commit, state}, {encounter}) => {
    const query = {
      encounter,
      $or: [
        {type: 'ent-note'},
        {type: 'ob-note'}
      ]
    }

    const { items } = await core.medical.records().find(query);
    commit('setSpecialtyFeatures', items);
  },
  getCurrentEncounterMedRecords: async ({dispatch, commit, state}, {encounter}) => {
    const query = {
      encounter,
      $or: [
        {type: 'vitals'},
        {type: 'ros'},
        {type: 'assessment'},
        {type: 'care-plan'},
        {type: 'medication-order'},
        {type: 'physical-exam'},
        {type: 'medical-procedure'}
      ],
      $populate: {
        medicalProcedureProviders: {
          service: 'personal-details',
          method: 'find',
          localKey: 'providers',
          foreignKey: 'id'
        },
        ref: {
          service: 'services',
          method: 'get',
          localKey: 'ref'
        }
      }
    }

    const { items } = await core.medical.records().find(query);
    commit('setVitals', items.filter(item => item.type === 'vitals'));
    commit('setROS', items.filter(item => item.type === 'ros'));
    commit('setImpressions', items.filter(item => item.subtype === 'impression'));
    commit('setDiagnosis', items.filter(item => item.subtype === 'diagnosis'));
    commit('setCarePlans', items.filter(item => item.type === 'care-plan'));
    commit('setProcedures', items.filter(item => item.type === 'medical-procedure'));
    commit('setMedicationOrders', items.filter(item => item.type === 'medication-order'));
    commit('setPhysicalExam', items.filter(item => item.type === 'physical-exam'));
    await dispatch('getDiagnosticOrders', encounter);
  },
  getDiagnosticOrders: async ({dispatch}, encounter) => {
    const promises = ['getLabOrders', 'getImagingOrders']
      .map(action => dispatch(action, encounter));
    await Promise.all(promises);
  },
  getLabOrders: async ({commit}, encounter) => {
    const { items } = await core.medical.records().find({
      encounter,
      type: 'lab-test-order',
      $populate: {
        results: {
          service: 'medical-records',
          method: 'find',
          localKey: 'id',
          foreignKey: 'order',
          type: 'lab-test-result'
        }
      }
    });
    console.log('medical-record#getLabOrders#items', items);
    const labOrders = items.map(item => ({
      ..._.omit(item, '$populated'),
      ...item.$populated
    }));
    const testIds = _(labOrders)
      .map(labOrder => labOrder.tests)
      .flatMap(tests => tests.map(test => test.id))
      .uniq()
      .value();
    const measureIds = _(labOrders)
      .map(labOrder => labOrder.results.items)
      .filter(_.negate(_.isEmpty))
      .flatMap(results => results.map(result => result.measure))
      .uniq()
      .value();

    const { items: diagnosticTests } = await core.diagnostic.tests()
      .find({id: {$in: testIds}});
    const { items: diagnosticMeasures } = await core.diagnostic.measures()
      .find({id: {$in: measureIds}});

    const labOrdersWithTests = _(labOrders)
      .map(labOrder => ({
        ...labOrder,
        results: _.map(labOrder.results, labResult => ({
          ...labResult,
          results: _.map(labResult.results, result => ({
            ...result,
            test: diagnosticTests.find(t => t.id === result.test),
            measure: diagnosticMeasures.find(m => m.id === result.measure)
          }))
        }))
      }))
      .value();
    console.log('medical-record#getLabOrders#labOrders', labOrdersWithTests);

    commit('setLabOrders', labOrdersWithTests);
  },
  getImagingOrders: async ({commit}, encounter) => {
    const { items } = await core.medical.records().find({
      encounter,
      type: 'imaging-test-order',
      $populate: {
        results: {
          service: 'medical-records',
          method: 'find',
          localKey: 'id',
          foreignKey: 'order',
          type: 'imaging-test-result'
        }
      }
    });
    console.log('medical-record#getImagingOrders#items', items);
    const imagingOrders = items.map(item => ({
      ..._.omit(item, '$populated'),
      ...item.$populated
    }));
    const testIds = _(imagingOrders)
      .map(imagingOrder => imagingOrder.tests)
      .flatMap(tests => tests.map(test => test.id))
      .uniq()
      .value();
    const { items: diagnosticTests } = await core.diagnostic.tests()
      .find({id: {$in: testIds}});

    const imagingOrdersWithTests = _(imagingOrders)
      .map(imagingOrder => ({
        ...imagingOrder,
        results: _.map(imagingOrder.results, imagingResult => ({
          ...imagingResult,
          results: _.map(imagingResult.results, result => ({
            ...result,
            test: diagnosticTests.find(t => t.id === result.test)
          }))
        }))
      }))
      .value();
    console.log('medical-record#getImagingOrders#imagingOrders', imagingOrdersWithTests);

    commit('setImagingOrders', imagingOrdersWithTests);
  },
  updateChiefComplaint: async ({commit}, {encounter, facility, patient, text}) => {
    const data = await core.medical.records().findOne({type: 'chief-complaint', encounter});

    if(text.length === 0) {
      // remove if empty value
      console.log('removing...')
      await core.medical.records().remove(data.id);
      return;
    }

    let chiefComplaint;

    if(data && data.id) {
      // update if existing
      console.log('updating...')
      const result = await core.medical.records().update(data.id, {text});
      chiefComplaint = result[0];
    } else {
      // create if none
      const query = {
        type: 'chief-complaint',
        encounter,
        text,
        patient, 
        facility
      }
      chiefComplaint = await core.medical.records().create(query);
    }

    // TODO: remove for demo
    // commit('setMedicalRecord', {...chiefComplaint, type: 'chief-complaint'});
  },
  updateHPI: async ({commit}, {encounter, facility, patient, text}) => {
    const data = await core.medical.records().findOne({type: 'hpi', encounter});

    if(text.length === 0) {
      // remove if empty value
      console.log('removing...');
      await core.medical.records().remove(data.id);
      return;
    }

    if(data && data.id) {
      // update if existing
      console.log('updating...')
      await core.medical.records().update(data.id, {text});
    } else {
      // create if none
      const query = {
        type: 'hpi',
        encounter,
        text,
        patient, 
        facility
      }
      await core.medical.records().create(query);
    }
  },
  createMedicalRecord: async ({commit}, {type, facility, encounter, patient, data}) => {
    let payload = {
      type,
      facility, 
      encounter, 
      patient,
      ...data
    }
    console.log('medRecord#createMedicalRecord#text', payload.text)
    await core.medical.records().create(payload);
  },
  updateMedicalRecord: async ({commit}, {type, data, prescription, oldMeds = [], newMeds = []}) => {
    if(!type) 
      throw new Error('Type is required!');

    if(!prescription) {
      // update record if record is not prescription.
      await core.medical.records().update(data.id, _.omit(data, ['id']));
    } else {
      // update prescriptions items/medicines.
      // Remove all exising medicines in record.
      await core.medical.records().update(data.id, {
        $pull: {
          items: {
            id: {
              $in: oldMeds.map(med => med.id)
            }
          }
        }
      });
      // Push all new medicine in record's items.
      await core.medical.records().update(data.id, {
        $addToSet: { 
          items : { 
            $each: newMeds
          }
        }
      });
    }
  },
  updateDiagnosticOrder: async ({commit}, {data, oldTests = [], newTests = []}) => {
    await core.medical.records().update(data.id, _.omit(data, ['id', 'type']));
    await core.medical.records().update(data.id, {
      $pull: {
        tests: {
          id: {
            $in: oldTests.map(test => test.id)
          }
        }
      }
    });
    await core.medical.records().update(data.id, {
      $addToSet: { 
        tests : { 
          $each: newTests
        }
      }
    });
  },
  updateDiagnosis: async ({commit}, {data, oldProcedures = [], newProcedures = []}) => {
    await core.medical.records().update(data.id, _.omit(data, ['id', 'type', 'subtype']));
    await core.medical.records().update(data.id, {
      $pull: {
        recommendedProcedures: {
          id: {
            $in: oldProcedures.map(procedure => procedure.id)
          }
        }
      }
    });
    await core.medical.records().update(data.id, {
      $addToSet: { 
        recommendedProcedures : { 
          $each: newProcedures
        }
      }
    });
  },
  removeMedicalRecord: async ({commit}, id) => {
    await core.medical.records().remove(id);
  }
};

const mutations = {
  setMedicalRecord: (s, val) => {
    s[val.type] = val;
  },
  setMedicalHistories: (s, val) => {
    s.medicalHistories = val.sort((a, b) => b.createdAt - a.createdAt);
  },
  setVitals: (s, val) => {
    s.vitals = val.sort((a, b) => b.createdAt - a.createdAt);
  },
  setSpecialtyFeatures: (s, val) => {
    s.specialtyFeatures = val.sort((a, b) => b.createdAt - a.createdAt);
  },
  setROS: (s, val) => {
    s.ros = val.sort((a, b) => b.createdAt - a.createdAt);
  },
  setImpressions: (s, val) => {
    s.impressions = val.sort((a, b) => b.createdAt - a.createdAt);
  },
  setDiagnosis: (s, val) => {
    s.diagnosis = val.sort((a, b) => b.createdAt - a.createdAt);
  },
  setPhysicalExam: (s, val) => {
    s.physicalExams = val.sort((a, b) => b.createdAt - a.createdAt);
  },
  setCarePlans: (s, val) => {
    s.carePlans = val.sort((a, b) => b.createdAt - a.createdAt);
  },
  setMedicationOrders: (s, val) => {
    s.medicationOrders = val.sort((a, b) => b.createdAt - a.createdAt);
  },
  setLabOrders: (s, val) => {
    s.labOrders = val.sort((a, b) => b.createdAt - a.createdAt);
  },
  setImagingOrders: (s, val) => {
    s.imagingOrders = val.sort((a, b) => b.createdAt - a.createdAt);
  },
  setProcedures: (s, val) => {
    s.procedures = val.sort((a, b) => b.createdAt - a.createdAt);
  },
  pushSelectedMedicationOrderMedicines: (s, val) => {
    s.selectedMedicationOrderMedicines.push(val);
  },
  setSelectedMedicationOrderMedicines: (s, val) => {
    s.selectedMedicationOrderMedicines = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
