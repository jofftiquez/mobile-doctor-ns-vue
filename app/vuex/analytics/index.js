import { core } from '@mycure/sdk';
import subDays from 'date-fns/sub_days';
import eachDays from 'date-fns/each_day';
import { format } from 'date-fns';

const state = {
  patientVisit: [],
  sexSummary: [
    {sex: 'Male', total: 0},
    {sex: 'Female', total: 0},
  ]
};

const getters = {
  patientVisit: (s) => s.patientVisit,
  sexSummary: (s) => s.sexSummary,
};

const actions = {
  getPatientVisit: async ({commit}, {facility, from, to}) => {

    if(!from || !to) {
      from = subDays(new Date(), 7);
      from = new Date(from).getTime();
      to = new Date().getTime();
    }

    const dates = eachDays(from, to).map(date => format(date, 'MM-DD-YY'));

    const query = {
      facility,
      finishedAt: { $gte: from, $lte: to },
      $dateFields: ['finishedAt.$gte', 'finishedAt.$lte'],
      type: 'outpatient',
      $analytics: { type: 'return-type', group: true }
    }
    const { items } = await core.medical.encounters().find(query);

    const results = items.map(item => {
      return {
        date: format(item.date, 'MM-DD-YY'), 
        returning: item.returning, 
        firstTime: item.firstTime
      }
    });

    const resultsGrouped = _(results)
      .groupBy('date')
      .map((obj, key) => { 
        return { 
          date: key, 
          returning: _.map(obj, 'returning').reduce((a,b) => a + b), 
          firstTime: _.map(obj, 'firstTime').reduce((a,b) => a + b)
        } 
      }).value();
    
    dates.forEach(date => {
      const res = resultsGrouped.filter(item => date === item.date);
      if(!res.length) {
        resultsGrouped.push({
          date, 
          returning: 0,
          firstTime: 0
        })
      }
    });

    commit('setPatientVisit', resultsGrouped);
  },
  getSexSummary: async ({commit},  {facility, from, to}) => {
    if(!from || !to) {
      from = subDays(new Date(), 7);
      from = new Date(from).getTime();
      to = new Date().getTime();
    }

    const dates = eachDays(from, to).map(date => format(date, 'MM-DD-YY'));

    const query = {
      facility,
      finishedAt: { $gte: from, $lte: to },
      $dateFields: ['finishedAt.$gte', 'finishedAt.$lte'],
      type: 'outpatient',
      $analytics: { type: 'patient-sex-distribution' }
    }
    const { items } = await core.medical.encounters().find(query);

    const maleTotal = items.map(item => item.male || 0)
      .reduce((a, b) => a + b);

    const femaleTotal = items.map(item => item.female || 0)
      .reduce((a, b) => a + b);

    const final = [
      {sex:'Male', total: maleTotal},
      {sex:'Female', total: femaleTotal}
    ]
    
    commit('setSexSummary', final);
  },
  getSexTransactionSummary: async ({commit},  {facility, from, to}) => {
    if(!from || !to) {
      from = subDays(new Date(), 7);
      from = new Date(from).getTime();
      to = new Date().getTime();
    }

    const dates = eachDays(from, to).map(date => format(date, 'MM-DD-YY'));

    const query = {
      facility,
      finishedAt: { $gte: from, $lte: to },
      $dateFields: ['finishedAt.$gte', 'finishedAt.$lte'],
      type: 'outpatient',
      $analytics: { type: 'patient-sex-distribution' }
    }
    const { items } = await core.medical.encounters().find(query);

    const maleTotal = items.map(item => item.male || 0)
      .reduce((a, b) => a + b);

    const femaleTotal = items.map(item => item.female || 0)
      .reduce((a, b) => a + b);

    const final = [
      {sex:'Male', total: maleTotal},
      {sex:'Female', total: femaleTotal}
    ]
    
    commit('setSexSummary', final);
  },
  getEncountersSummary: async ({commit, dispatch}, {facility, from, to}) => {
    if(!from || !to) {
      from = subDays(new Date(), 7);
      from = new Date(from).getTime();
      to = new Date().getTime();
    }

    const dates = eachDays(from, to).map(date => format(date, 'MM-DD-YY'));

    const currentUser = await dispatch('auth/getCurrentUser', null, {root: true});
    const userUID = currentUser.uid; 

    const query = {
      facility,
      finishedAt: { $gte: from, $lte: to },
      $dateFields: ['finishedAt.$gte', 'finishedAt.$lte'],
      type: 'outpatient',
      $prequeryOr: [
        {
          service: 'medical-encounters', extractKey: 'id', resKey: 'id', resOps: '$in',
          query: {
            facility, 
            $or: [
              { doctor: userUID ? userUID : { $exists: true } },
              { doctors: userUID ? userUID : { $exists: true, gt: {$size: 0} } },
              { createdBy: userUID ? userUID : { $exists: true } }
            ]
          }
        }, 
        {
          service: 'billing-items', 
          extractKey: 'invoice', resKey: 'id', resOps: '$in',
          query: {
            facility, 
            $or: [
              { 'commissions.provider': userUID ? userUID : { $exists: true } },
              { providers: userUID ? userUID : { $exists: true, gt: {$size: 0} } },
              { createdBy: userUID ? userUID : { $exists: true } }
            ]
          }
        }, 
        {
          service: 'medical-records', extractKey: 'encounter', resKey: 'id', resOps: '$in',
          query: {
            facility, $or: [
              { providers: userUID ? userUID : { $exists: true, gt: {$size: 0} } },
              { createdBy: userUID ? userUID : { $exists: true } }
            ]
          }
        }
      ],
      $analytics: { type: 'return-type', group: true }
    }
    const { items } = await core.medical.encounters().find(query);

    console.log('encounters ======> ', items);
  }
};

const mutations = {
  setPatientVisit: (s, val) => {
    s.patientVisit = val.sort((a, b) => new Date(a.date) - new Date(b.date));
  },
  setSexSummary: (s, val) => {
    s.sexSummary = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
