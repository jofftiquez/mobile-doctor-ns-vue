import { core } from '@mycure/sdk';

const state = {
  proceduresDefaults: [],
};

const getters = {
  proceduresDefaults: (s) => s.proceduresDefaults,
};

const actions = {
  getProcedures: async ({commit}, {facility, filters}) => {
    const query = {
      facility,
      $or: [
        { type: { $in: filters } },
        { subtype: { $in: filters } },
      ],
      $populate: {
        coverages: {
          service: 'insurance-coverages',
          key: 'coverages',
          $populate: {
            _contractDetails: { service: 'insurance-contracts', key: 'contract' },
          }
        }
      }
    };

    commit('setProceduresDefaults', []);
    const { items } = await core.serv.services().find(query);
    commit('setProceduresDefaults', items);
  }
};

const mutations = {
  setProceduresDefaults: (s, val) => {
    s.proceduresDefaults = val;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
