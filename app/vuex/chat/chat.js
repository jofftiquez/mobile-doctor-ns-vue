import { core } from '@mycure-sdk';
import { MESSAGE_ALLOWABLE_KEYS } from './constants.js';
import _ from 'lodash';

const state = {
  queueSubscription: null,
  queue: [],
  roomsSubscription: null,
  rooms: [],
  sessionSubscription: null,
  session: null,
  roomSubscription: null,
  room: null,
  messageSubscription: null,
  messages: []
};

const getters = {
  isQueued: ({ session }) => session && !session.startedAt,
  isAccepted: ({ session }) => session && session.acceptedAt,
  isRejected: ({ session }) => session && session.rejectedAt
};

const actions = {
  async watchQueue ({ dispatch, commit }, { account, organization }) {
    dispatch('unwatchQueue');

    const result = await core.queue.queues().findOne({
      organization,
      tags: account,
      type: 'telemed-patient-pool'
    });
    const subscription = result.watch()
      .subscribe(queue => commit('setQueue', queue));
    commit('setQueueSubscription', subscription);
  },
  async unwatchQueue ({ state: { queueSubscription }, commit }) {
    if (!queueSubscription) return;
    await queueSubscription.unsubscribe();
    commit('setQueueSubscription', null);
  },
  async startSession (context, id) {
    await core.chat.sessions().updateOne({ id }, { start: true });
  },
  async acceptSession (context, id) {
    await core.chat.sessions().updateOne({ id }, { accept: true });
  },
  async rejectSession (context, { id, reason }) {
    await core.chat.sessions().updateOne({ id }, { reject: reason || true });
  },
  async fetchRooms ({ commit }, account) {
    const { items: rooms } = await core.chat.rooms().find({
      participants: account
    });
    commit('setRooms', rooms);
  },
  async watchRooms ({ dispatch, commit }, account) {
    dispatch('unwatchRooms');

    const result = await core.chat.rooms().find({ participants: account });
    const subscription = result.watch()
      .subscribe(rooms => commit('setRooms', rooms));
    commit('setRoomsSubscription', subscription);
  },
  async unwatchRooms ({ state: roomsSubscription, commit }) {
    if (!roomsSubscription) return;
    await roomsSubscription.unsubscribe();
    commit('setRoomsSubscription', null);
  },
  async queue ({ dispatch, commit }, { account, organization }) {
    const telemedQueue = await core.queue.queues().findOne({
      organization,
      type: 'telemed-patient-pool'
    });
    if (!telemedQueue) {
      throw Error(`No telemed queue for organization: ${organization}`);
    }

    const queueItem = {
      subjectType: 'account',
      subject: account,
      queue: telemedQueue.id,
      organization
    };
    const { id } = await core.queue.items().create(queueItem);

    dispatch('watchSession', id);
    const { room } = await core.chat.sessions().get(id);
    dispatch('watchRoom', room);
  },
  async dequeue ({ state: { session }, dispatch }) {
    if (!session) return;
    await core.queue.items().remove(session.id);
    dispatch('unwatchSession');
  },
  async watchSession ({ dispatch, commit }, id) {
    dispatch('unwatchSession');

    const result = await core.chat.sessions().get(id);
    const subscription = result.watch()
      .subscribe(session => commit('setSession', session));
    commit('setSessionSubscription', subscription);
  },
  async unwatchSession ({ state: { sessionSubscription }, commit }) {
    if (!sessionSubscription) return;
    await sessionSubscription.unsubscribe();
    commit('setSessionSubscription', null);
  },
  async fetchRoom ({ dispatch, commit }, id) {
    const room = await core.chat.rooms().get(id);
    commit('setRoom', room);
    dispatch('fetchMessages', id);
  },
  async watchRoom ({ dispatch, commit }, id) {
    dispatch('unwatchRoom');

    const result = await core.chat.rooms().get(id);
    const subscription = result.watch()
      .subscribe(room => commit('setRoom', room));
    commit('setRoomSubscription', subscription);

    dispatch('watchMessages', id);
  },
  async unwatchRoom ({ state: roomSubscription, dispatch, commit }) {
    if (!roomSubscription) return;
    await roomSubscription.unsubscribe();
    commit('setRoomSubscription', null);

    dispatch('unwatchMessages');
  },
  async fetchMessages ({ commit }, room) {
    const { items: messages } = await core.chat.messages().find({ room });
    commit('setMessages', messages);
  },
  async watchMessages ({ dispatch, commit }, room) {
    dispatch('unwatchMessages');

    const result = await core.chat.messages().find({ room });
    const subscription = result.watch()
      .subscribe(messages => commit('setMessages', messages));
    commit('setMessageSubscription', subscription);
  },
  async unwatchMessages ({ state: { messageSubscription }, commit }) {
    if (!messageSubscription) return;
    await messageSubscription.unsubscribe();
    commit('setMessageSubscription', null);
  },
  async sendMessage ({ state: { room, session } }, message) {
    await core.chat.messages().create({
      room,
      session,
      ..._.pick(message, MESSAGE_ALLOWABLE_KEYS)
    });
  },
  async deinit ({ dispatch }) {
    await dispatch('unwatchRooms');
    await dispatch('unwatchSession');
    await dispatch('unwatchRoom');
    await dispatch('unwatchMessages');
  }
};

const mutations = {
  setQueueSubscription (s, val) {
    s.queueSubscription = val;
  },
  setQueue (s, val) {
    s.queue = val;
  },
  setRoomsSubscription (s, val) {
    s.roomsSubscription = val;
  },
  setRooms (s, val) {
    s.rooms = val;
  },
  setSessionSubscription (s, val) {
    s.sessionSubscription = val;
  },
  setSession (s, val) {
    s.session = val;
  },
  setRoomSubscription (s, val) {
    s.roomSubscription = val;
  },
  setRoom (s, val) {
    s.room = val;
  },
  setMessageSubscription (s, val) {
    s.messageSubscription = val;
  },
  setMessages (s, val) {
    s.messages = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
