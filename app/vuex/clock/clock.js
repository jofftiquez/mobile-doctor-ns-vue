// has startAt na walang endAt

import { core } from '@mycure/sdk';

const state = {
  clock: null,
  isChatOnline: false
};

const getters = {
  clock: (s) => s.clock,
  isChatOnline: (s) => s.isChatOnline
};

const actions = {
  getClock: async ({commit}, {organization, uid}) => {
    const query = {
      organization,
      uid,
      startAt: {
        $exists: true
      },
      endAt: {
        $exists: false
      }
    }

    const data = await core.clocks().findOne(query);
    if(!data) {
      commit('setChatStatus', false);
    } else {
      commit('setChatStatus', true);
      commit('setClock', data);
    }
    console.log('clocks', data);
  },
  clockIn: async ({commit, dispatch}, {organization}) => {
    const data = await core.clocks().create({type: 'attendance', organization});
    commit('setChatStatus', true);
    commit('setClock', data);
  },
  clockOut: async ({commit, dispatch}, {id}) => {
    await core.clocks().update(id, {end: true});
    commit('setChatStatus', false);
    commit('setClock', null);
  }
};

const mutations = {
  setChatStatus: (s, val) => {
    s.isChatOnline = val;
  },
  setClock: (s, val) => {
    s.clock = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
