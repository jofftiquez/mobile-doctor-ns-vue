import { core } from '@mycure/sdk';
import { omit } from 'lodash';
import { 
  setCurrentUser
} from '~/firebase';

const state = {
  account: null,
  personalDetails: {
    name: {
      middleName: ''
    }
  },
  professionalDetails: null
};

const getters = {
  account: (s) => s.account,
  personalDetails: (s) => s.personalDetails,
  professionalDetails: (s) => s.professionalDetails
};

const actions = {
  getAccount: async ({commit}, uid) => {
    // TODO: sdk get account
    const data = '';
    commit('setAccount', data);
  },
  updateAccount: async ({commit}, {uid, payload}) => {
    // TODO: sdk update account
    const data = '';
    commit('setAccount', data);
  },
  getPersonalDetails: async ({commit}, uid) => {
    const data = await core.personalDetails().get(uid);
    return data;
  },
  updatePersonalDetails: async ({commit}, payload) => {
    const uid = payload.uid;
    const data = _.omit(payload, ['uid', 'doc_verified', 'id', 'picURL'])
    const newPersonalDetails = await core.personalDetails().update(uid, data);
    newPersonalDetails.uid = uid;
    await setCurrentUser(newPersonalDetails);
  },
  getProfessionDetails: async ({commit}, uid) => {
    // TODO: sdk get professionDetails
    const data = '';
    // commit('setProfessionDetails', data);
  },
  updateProfessionDetails: async ({commit}, {uid, payload}) => {
    // TODO: sdk get professionDetails
    const data = '';
    // commit('setProfessionDetails', data);
  },
  wipe: ({commit}) => {
    commit('setAccount', null);
    commit('setPersonalDetails', null);
    commit('setProfessionDetails', null);
  }
};

const mutations = {
  setAccount: (s, val) => {
    s.account = val;
  },
  setPersonalDetails: (s, val) => {
    s.personalDetails = val;
  },
  setProfessionDetails: (s, val) => {
    s.professionalDetails = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
